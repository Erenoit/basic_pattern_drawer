#include <stdio.h>

void writeToFilePPM(char *file_path, size_t h, size_t w, int pixels[h][w]);
void fillOneColor  (size_t h, size_t w, int pixels[h][w], int color);
void stripesPattern(size_t h, size_t w, int pixels[h][w], int bg, int fg, size_t size);
void checkerPattern(size_t h, size_t w, int pixels[h][w], int bg, int fg, size_t size);
void solidCircle   (size_t h, size_t w, int pixels[h][w], int bg, int fg, size_t radius);
void hollowCircle  (size_t h, size_t w, int pixels[h][w], int bg, int fg, size_t radius, size_t border_thickness);

int main(void) {
    size_t const HEIGHT     = 1024;
    size_t const WIDTH      = 1024;
    size_t const RADIUS     = WIDTH < HEIGHT ? WIDTH / 2 : HEIGHT / 2;
    //size_t const RADIUS     = 256;
    size_t const THICKNESS  = 20;
    size_t const SIZE       = 8;
    int const FOREGROUND    = 0xFF00FF;
    int const BACKGROUND    = 0x000000;
    int pixels[HEIGHT][WIDTH];

    fillOneColor(HEIGHT, WIDTH, pixels, FOREGROUND);
    writeToFilePPM("one_color.ppm", HEIGHT, WIDTH, pixels);

    stripesPattern(HEIGHT, WIDTH, pixels, BACKGROUND, FOREGROUND, SIZE);
    writeToFilePPM("stripes.ppm", HEIGHT, WIDTH, pixels);

    checkerPattern(HEIGHT, WIDTH, pixels, BACKGROUND, FOREGROUND, SIZE);
    writeToFilePPM("checker.ppm", HEIGHT, WIDTH, pixels);

    solidCircle(HEIGHT, WIDTH, pixels, BACKGROUND, FOREGROUND, RADIUS);
    writeToFilePPM("solid_circle.ppm", HEIGHT, WIDTH, pixels);

    hollowCircle(HEIGHT, WIDTH, pixels, BACKGROUND, FOREGROUND, RADIUS, THICKNESS);
    writeToFilePPM("hollow_circle.ppm", HEIGHT, WIDTH, pixels);
}

void writeToFilePPM(char *file_path, size_t h, size_t w, int pixels[h][w]) {
    FILE *f;
    f = fopen(file_path, "wb");

    fprintf(f, "P6\n%lu %lu\n255\n", w, h); // P6 is PPM type

    for (int i = 0; i < h; i++) {
        for (int j = 0; j < w; j++) {
            char color[3];
            color[0] = pixels[i][j] >> 8 * 2 & 0xFF;
            color[1] = pixels[i][j] >> 8 * 1 & 0xFF;
            color[2] = pixels[i][j] >> 8 * 0 & 0xFF;

            fwrite(color, 1, 3, f);
        }
    }

    fclose(f);
}

void fillOneColor(size_t h, size_t w, int pixels[h][w], int color) {
    for (int i = 0; i < h; i++) {
        for (int j = 0; j < w; j++) {
            pixels[i][j] = color;
        } 
    }
}

void stripesPattern(size_t h, size_t w, int pixels[h][w], int bg, int fg, size_t size) {
    for (size_t i = 0; i < h; i++) {
        for (size_t j = 0; j < w; j++) {
            pixels[i][j] = ((i + j) / size) & 1 ? fg : bg;
        }
    }
}

void checkerPattern(size_t h, size_t w, int pixels[h][w], int bg, int fg, size_t size) {
    for (size_t i = 0; i < h; i++) {
        for (size_t j = 0; j < w; j++) {
            pixels[i][j] = (!(i / size & 1) && (j / size & 1)) || ((i/size & 1) && !(j/size & 1)) ? fg : bg;
        }
    }
}

void solidCircle(size_t h, size_t w, int pixels[h][w], int bg, int fg, size_t radius) {
    long long cx = w;
    long long cy = h;
    long long r  = radius * 2;

    for (size_t i = 0; i < h; i++) {
        for (size_t j = 0; j < w; j++) {
            long long x = cx - 2 * i - 1;
            long long y = cy - 2 * j - 1;

            pixels[i][j] = x*x + y*y <= r*r ? fg : bg;
        }
    }
}

// Midpoint Circle Algorithm
// https://www.wikiwand.com/en/Midpoint_circle_algorithm
void hollowCircle  (size_t h, size_t w, int pixels[h][w], int bg, int fg, size_t radius, size_t border_thickness) {
    long long r = radius * 2;
    long long cx = w;
    long long cy = h;

    fillOneColor(h, w, pixels, bg);

    long long x = 1;
    long long y = r - 1;
    while (x < y) {
        while (x*x + y*y > r*r) {
            if ((x*x + y*y) - r*r < r*r - (x*x + (y-1)*(y-1))) {
                break;
            }
            else {
                y -= 2;
            }
        }

        for (size_t i = 0; i < border_thickness; i++) {
            size_t nx = (x + cx) / 2;
            size_t ny = ((cx - y) / 2) + i;

            pixels[ny][nx]                 = fg;
            pixels[nx][ny]                 = fg;
            pixels[h - 1 - ny][nx]         = fg;
            pixels[nx][h - 1 - ny]         = fg;
            pixels[w - 1 - nx][ny]         = fg;
            pixels[ny][w - 1 - nx]         = fg;
            pixels[h - 1 - ny][w - 1 - nx] = fg;
            pixels[w - 1 - nx][h - 1 - ny] = fg;
        }
        x += 2;
    }
}
