# C Code For Generating Some Basic Patterns/Shapes

C script that generates several petterns/shapes. The idea is inspired from [this repo](https://github.com/tsoding/midpoint-circle-visualization).

## Quick start
```shell
$ gcc main.c
$ ./a.out
$ feh --force-alising -B black *.ppm &
```

## What is ppm
*see also: https://www.wikiwand.com/en/Netpbm*

PPM is a basic image format. No libraries are needed for generating an ppm file. Example ppm file:
```
P6         # Magic Number
1920 1080  # Resolution of Image
255        # The maximum value of one of the R, G or B colors. It cannot be more then 255.
# Content
```
